package studyguide;

/**
 *
 * @author geraldfrilot
 */
public class Geology {

    public String[] Questions() {
        String[] questions = {"Geology is the scientific study of Earth and it's processes", "Plate tectonics is a theory", "The water table"
            + " will be low where the surface is high and high where the surface is low",
            "Streak is a physical property associated with rock identification", "The lithosphere is composed of the crust and core"};

        return questions;
    }

    public String[] Answers() {
        String[] answers = {"True", "True", "False", "True", "False"};

        return answers;
    }
}
