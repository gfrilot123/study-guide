/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studyguide;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author geraldfrilot
 */
public class RegisterController implements Initializable{

    @FXML
    private TextField fname;
    @FXML
    private TextField lname;
    @FXML
    private TextField age;
    @FXML
    private TextField gender;
    @FXML
    private TextField user_name;
    @FXML
    private TextField pword;
    @FXML
    private TextField style;
    @FXML
    private Button register_now;
    
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void RegisterUser(ActionEvent event) throws ClassNotFoundException, SQLException {
        

        String Fname = fname.getText();
        String Lname = lname.getText();
        String Age = age.getText();
        String Gender = gender.getText();
        String Uname = user_name.getText();
        String Pass = pword.getText();
        String Style = style.getText();
        
        // Load the Driver
        Class.forName("com.mysql.cj.jdbc.Driver");
        System.out.println("Driver Connected");
        
        // Connect to database
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/study_guide?serverTimezone=UTC", "root", "password");
        System.out.println("Database Connected");
        
         // Create Statement
         Statement statement= connection.createStatement();
         String sql = "INSERT INTO users (`firstName`,`lastName`,`age`,`gender`,`username`,`secret`,`learningStyle`) "
                 + "VALUES('"+Fname+"','"+Lname+"','"+Age+"','"+Gender+"','"+Uname+"','"+Pass+"','"+Style+"')";
         
         // Execute Statement
         try{
              statement.executeUpdate(sql);
         }
         catch(Exception e){
             
            e.printStackTrace();
         }
         
         //  Close this stage and redirect to sign in
          
          
          ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }
    
}
