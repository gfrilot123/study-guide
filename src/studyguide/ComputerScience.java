package studyguide;

/**
 *
 * @author geraldfrilot
 */
public class ComputerScience {

    public String[] Questions() {
        String[] questions = {"The CPU is responsible for executing instructions for the computer ", "Main memory is a place where programs and data"
                + " is stored temporarily during processing",
            "Writing code is the first step in the waterfall model of developing software",
            "The C programming language is not considered high-level because of it's age", "A C program begins with a section of preprocessor directives"};

        return questions;
    }

    public String[] Answers() {
        String[] answers = {"True", "True", "False", "False", "True"};

        return answers;
    }
}
