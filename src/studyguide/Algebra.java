package studyguide;

/**
 *
 * @author geraldfrilot
 */
public class Algebra {

    public String[] Questions() {
        String[] questions = {"Is (-4) * (-44) positive or negative?", "Is -56 ÷ -8 positive or negative?", "Is 33 × 59 positive or negative?",
            "Is -1,960 ÷ (-2) positive or negative?", "Is -168 ÷ -4 positive or negative?"};

        return questions;
    }

    public String[] Answers() {
        String[] answers = {"Positive", "Positive", "Positive", "Positive", "Positive"};

        return answers;
    }
}
