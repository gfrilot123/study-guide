/*
 * 
 * 
 */
package studyguide;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;

/**
 *
 * @author mac
 */
public class NewProjectFXMLController implements Initializable {

    String[] q1 = null;

    String[] color_array = {"-fx-text-fill: green", "-fx-text-fill: red", "-fx-text-fill: blue", "-fx-text-fill: orange", "-fx-text-fill: brown"};

    Algebra alg_test;

    Geology geo_test;

    History his_test;

    ComputerScience cmps_test;

    int count = 0;

    String style_chosen;

    String subject_chosen;

    String[] results = new String[5];

    VisualLearner vis_learner;

    @FXML
    private GridPane p1Grid;
    @FXML
    private ToggleButton toggleQuestionButton;
    @FXML
    private TextField flashcardQuestion;
    @FXML
    private MenuButton learning_option;
    @FXML
    private MenuButton subject_option;
    @FXML
    private MenuItem audible_learner;
    @FXML
    private MenuItem kinetic_learner;
    @FXML
    private MenuItem verbal_learner;
    @FXML
    private MenuItem visual_learner;
    @FXML
    private MenuItem cmps_chosen;
    @FXML
    private MenuItem math_chosen;
    @FXML
    private MenuItem history_chosen;
    @FXML
    private MenuItem geo_chosen;
    @FXML
    private RadioButton reset_state;
    @FXML
    private Label desired_learning_label;
    @FXML
    private Label desired_subject_label;
    @FXML
    private Button start;
    @FXML
    private RadioButton positive;
    @FXML
    private RadioButton negative;
    @FXML
    private ToggleButton changeQuestion;

    /**
     *
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        toggleQuestionButton.setShape(new Circle(1.5));
        toggleQuestionButton.setMaxSize(55, 55);
        toggleQuestionButton.setVisible(false);
        start.setShape(new Circle(1.5));
        start.setMaxSize(65, 65);
        start.setVisible(false);

        changeQuestion.setShape(new Circle(1.5));
        changeQuestion.setMaxSize(65, 65);

    }

    @FXML
    private void switchQuestion(ActionEvent event) {

        learning_option.setVisible(false);
        subject_option.setVisible(false);
        desired_learning_label.setVisible(false);
        desired_subject_label.setVisible(false);
        reset_state.setVisible(false);
        flashcardQuestion.setText("Press Start to Begin!");
        toggleQuestionButton.setVisible(false);
        start.setVisible(true);

    }

    @FXML
    private void SetAudible(ActionEvent event) {

        System.out.println("Audible Chosen");
        flashcardQuestion.setText("Audible Option Selected ");
        learning_option.setDisable(true);
        if (learning_option.isDisabled() && subject_option.isDisabled()) {
            toggleQuestionButton.setVisible(true);
        }
    }

    @FXML
    private void SetKinetic(ActionEvent event) {
        System.out.println("Kinetic Chosen");
        flashcardQuestion.setText("Kinetic Option Selected ");
        style_chosen = "Kinetic";
        learning_option.setDisable(true);
        if (learning_option.isDisabled() && subject_option.isDisabled()) {
            toggleQuestionButton.setVisible(true);
        }

    }

    @FXML
    private void SetVerbal(ActionEvent event) {
        System.out.println("Verbal Chosen");
        flashcardQuestion.setText("Verbale Option Selected ");
        style_chosen = "Verbal";
        learning_option.setDisable(true);
        if (learning_option.isDisabled() && subject_option.isDisabled()) {
            toggleQuestionButton.setVisible(true);
        }

    }

    @FXML
    private void SetVisual(ActionEvent event) {
        System.out.println("Visual Chosen");
        flashcardQuestion.setText("Visual Option Selected");
        style_chosen = "Visual";
        learning_option.setDisable(true);
        if (learning_option.isDisabled() && subject_option.isDisabled()) {
            toggleQuestionButton.setVisible(true);
        }

    }

    @FXML
    private void SetCmps(ActionEvent event) {
        System.out.println("Computer Science Selected");
        subject_chosen = "Computer Science";
        flashcardQuestion.appendText("/ Computer Science Course Selected!");
        subject_option.setDisable(true);
        if (learning_option.isDisabled() && subject_option.isDisabled()) {
            toggleQuestionButton.setVisible(true);
        }
    }

    @FXML
    private void SetMath(ActionEvent event) {
        System.out.println("Math Selected");
        subject_chosen = "Math";
        flashcardQuestion.appendText("/  Math Course Selected!");
        subject_option.setDisable(true);
        if (learning_option.isDisabled() && subject_option.isDisabled()) {
            toggleQuestionButton.setVisible(true);
        }
    }

    @FXML
    private void SetHistory(ActionEvent event) {
        System.out.println("History Selected");
        subject_chosen = "History";
        flashcardQuestion.appendText("/ History Course Selected!");
        subject_option.setDisable(true);
        if (learning_option.isDisabled() && subject_option.isDisabled()) {
            toggleQuestionButton.setVisible(true);
        }
    }

    @FXML
    private void SetGeology(ActionEvent event) {
        System.out.println("Geology Selected");
        subject_chosen = "Geology";
        flashcardQuestion.appendText("/ Geology Course Selected!");
        subject_option.setDisable(true);
        if (learning_option.isDisabled() && subject_option.isDisabled()) {
            toggleQuestionButton.setVisible(true);
        }

    }

    @FXML
    private void ResetOptions(ActionEvent event) {

        if (learning_option.isDisabled()) {
            learning_option.setDisable(false);
        }

        if (subject_option.isDisabled()) {
            subject_option.setDisable(false);
        }

        toggleQuestionButton.setVisible(false);
        flashcardQuestion.setText("WELCOME BACK!");
        reset_state.setSelected(false);

    }

    @FXML
    private void BeginTest(ActionEvent event) {

        System.out.println("Test Starting");
        flashcardQuestion.setText("");

        // Visual Style Chosen
        if (style_chosen == "Visual") {
            switch (subject_chosen) {

                // Computer Science option
                case ("Computer Science"):
                    System.out.println("Visual Chosen");
                    vis_learner = new VisualLearner();
                    cmps_test = new ComputerScience();
                    q1 = cmps_test.Questions();

                    if (count < q1.length) {
                        start.setVisible(false);
                        changeQuestion.setVisible(true);
                        flashcardQuestion.setText(q1[count].toString());
                        flashcardQuestion.setStyle("-fx-text-fill: purple");
                        positive.setText("True");
                        positive.setStyle("-fx-text-fill: white");
                        positive.setVisible(true);

                        negative.setText("False");
                        negative.setStyle("-fx-text-fill: white");
                        negative.setVisible(true);

                    }
                    break;

                // Math option
                case ("Math"):

                    System.out.println("Visual Chosen");
                    vis_learner = new VisualLearner();
                    alg_test = new Algebra();
                    q1 = alg_test.Questions();

                    if (count < q1.length) {
                        start.setVisible(false);
                        changeQuestion.setVisible(true);
                        flashcardQuestion.setText(q1[count].toString());
                        flashcardQuestion.setStyle("-fx-text-fill: purple");
                        positive.setVisible(true);
                        negative.setVisible(true);

                    }

                    break;

                // Geology option
                case ("Geology"):

                    System.out.println("Visual Chosen");
                    vis_learner = new VisualLearner();
                    geo_test = new Geology();
                    q1 = geo_test.Questions();

                    if (count < q1.length) {
                        start.setVisible(false);
                        changeQuestion.setVisible(true);
                        flashcardQuestion.setText(q1[count].toString());
                        flashcardQuestion.setStyle("-fx-text-fill: purple");

                        positive.setText("True");
                        positive.setStyle("-fx-text-fill: white");
                        positive.setVisible(true);

                        negative.setText("False");
                        negative.setStyle("-fx-text-fill: white");
                        negative.setVisible(true);

                    }

                    break;

                // History option
                case ("History"):
                    System.out.println("Visual Chosen");
                    vis_learner = new VisualLearner();
                    his_test = new History();
                    q1 = his_test.Questions();

                    if (count < q1.length) {
                        start.setVisible(false);
                        changeQuestion.setVisible(true);
                        flashcardQuestion.setText(q1[count].toString());
                        flashcardQuestion.setStyle("-fx-text-fill: purple");

                        positive.setText("True");
                        positive.setStyle("-fx-text-fill: white");
                        positive.setVisible(true);

                        negative.setText("False");
                        negative.setStyle("-fx-text-fill: white");
                        negative.setVisible(true);

                    }
                    break;

            }

        }

    }

    @FXML
    private void PosSelected(ActionEvent event) {
        negative.setDisable(true);

    }

    @FXML
    private void NegSelected(ActionEvent event) {
        positive.setDisable(true);
    }

    @FXML
    private void NextQuestion(ActionEvent event) {

        // Math Chosen
        if (subject_chosen == "Math") {
            if (count < q1.length - 1) {

                flashcardQuestion.setText(q1[count].toString());
                flashcardQuestion.setStyle(color_array[count]);
                if (positive.isSelected() || negative.isSelected()) {
                    if (positive.isSelected()) {
                        results[count] = "Positive";
                    } else {
                        results[count] = "Negative";
                    }
                    count++;
                    flashcardQuestion.setText(q1[count].toString());
                    flashcardQuestion.setStyle(color_array[count]);
                    positive.setSelected(false);
                    positive.setDisable(false);
                    negative.setSelected(false);
                    negative.setDisable(false);

                }

            } else {
                if (positive.isSelected()) {
                    results[count] = "Positive";
                } else {
                    results[count] = "Negative";
                }
                for (String res : results) {
                    System.out.println(res);
                }
                changeQuestion.setVisible(false);
                positive.setVisible(false);
                negative.setVisible(false);

                String[] comparing = alg_test.Answers();
                int right = 0;
                int wrong = 0;
                for (int i = 0; i < results.length; i++) {
                    if (comparing[i] == results[i]) {
                        right++;
                    } else {
                        wrong++;
                    }
                }
                ArrayList correct = vis_learner.TotalCorrect(right, "Visual");
                ArrayList wrong_values = vis_learner.TotalMissed(wrong, "Visual");
                System.out.println(correct.get(correct.indexOf("Visual") - 1));
                System.out.println(correct.indexOf("Visual"));
                flashcardQuestion.setText("Congratulations you have completed the exam! You scored: " + right + " out of " + results.length);
            }
        }
        // Geology chosen
        if (subject_chosen == "Geology") {
            if (count < q1.length - 1) {

                flashcardQuestion.setText(q1[count].toString());
                flashcardQuestion.setStyle(color_array[count]);
                if (positive.isSelected() || negative.isSelected()) {
                    if (positive.isSelected()) {
                        results[count] = "True";
                    } else {
                        results[count] = "False";
                    }
                    count++;
                    flashcardQuestion.setText(q1[count].toString());
                    flashcardQuestion.setStyle(color_array[count]);
                    positive.setSelected(false);
                    positive.setDisable(false);
                    negative.setSelected(false);
                    negative.setDisable(false);

                }

            } else {
                if (positive.isSelected()) {
                    results[count] = "True";
                } else {
                    results[count] = "False";
                }
                for (String res : results) {
                    System.out.println(res);
                }
                changeQuestion.setVisible(false);
                positive.setVisible(false);
                negative.setVisible(false);

                String[] comparing = geo_test.Answers();
                int right = 0;
                int wrong = 0;
                for (int i = 0; i < results.length; i++) {
                    if (comparing[i] == results[i]) {
                        right++;
                    } else {
                        wrong++;
                    }
                }
                ArrayList correct = vis_learner.TotalCorrect(right, "Visual");
                ArrayList wrong_values = vis_learner.TotalMissed(wrong, "Visual");
                System.out.println(correct.get(correct.indexOf("Visual") - 1));
                System.out.println(correct.indexOf("Visual"));
                flashcardQuestion.setText("Congratulations you have completed the exam! You scored: " + right + " out of " + results.length);
            }

        }

        // History Chosen
        if (subject_chosen == "History") {
            if (count < q1.length - 1) {

                flashcardQuestion.setText(q1[count].toString());
                flashcardQuestion.setStyle(color_array[count]);
                if (positive.isSelected() || negative.isSelected()) {
                    if (positive.isSelected()) {
                        results[count] = "True";
                    } else {
                        results[count] = "False";
                    }
                    count++;
                    flashcardQuestion.setText(q1[count].toString());
                    flashcardQuestion.setStyle(color_array[count]);
                    positive.setSelected(false);
                    positive.setDisable(false);
                    negative.setSelected(false);
                    negative.setDisable(false);

                }

            } else {
                if (positive.isSelected()) {
                    results[count] = "True";
                } else {
                    results[count] = "False";
                }
                for (String res : results) {
                    System.out.println(res);
                }
                changeQuestion.setVisible(false);
                positive.setVisible(false);
                negative.setVisible(false);

                String[] comparing = his_test.Answers();
                int right = 0;
                int wrong = 0;
                for (int i = 0; i < results.length; i++) {
                    if (comparing[i] == results[i]) {
                        right++;
                    } else {
                        wrong++;
                    }
                }
                ArrayList correct = vis_learner.TotalCorrect(right, "Visual");
                ArrayList wrong_values = vis_learner.TotalMissed(wrong, "Visual");
                System.out.println(correct.get(correct.indexOf("Visual") - 1));
                System.out.println(correct.indexOf("Visual"));
                flashcardQuestion.setText("Congratulations you have completed the exam! You scored: " + right + " out of " + results.length);
            }

        }

        // Computer Science Chosen
        if (subject_chosen == "Computer Science") {
            if (count < q1.length - 1) {

                flashcardQuestion.setText(q1[count].toString());
                flashcardQuestion.setStyle(color_array[count]);
                if (positive.isSelected() || negative.isSelected()) {
                    if (positive.isSelected()) {
                        results[count] = "True";
                    } else {
                        results[count] = "False";
                    }
                    count++;
                    flashcardQuestion.setText(q1[count].toString());
                    flashcardQuestion.setStyle(color_array[count]);
                    positive.setSelected(false);
                    positive.setDisable(false);
                    negative.setSelected(false);
                    negative.setDisable(false);

                }

            } else {
                if (positive.isSelected()) {
                    results[count] = "True";
                } else {
                    results[count] = "False";
                }
                for (String res : results) {
                    System.out.println(res);
                }
                changeQuestion.setVisible(false);
                positive.setVisible(false);
                negative.setVisible(false);

                String[] comparing = cmps_test.Answers();
                int right = 0;
                int wrong = 0;
                for (int i = 0; i < results.length; i++) {
                    if (comparing[i] == results[i]) {
                        right++;
                    } else {
                        wrong++;
                    }
                }
                ArrayList correct = vis_learner.TotalCorrect(right, "Visual");
                ArrayList wrong_values = vis_learner.TotalMissed(wrong, "Visual");
                System.out.println(correct.get(correct.indexOf("Visual") - 1));
                System.out.println(correct.indexOf("Visual"));
                flashcardQuestion.setText("Congratulations you have completed the exam! You scored: " + right + " out of " + results.length);
            }

        }

    }

}
