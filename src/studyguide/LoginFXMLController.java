/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studyguide;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import java.sql.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author geraldfrilot
 */
public class LoginFXMLController implements Initializable {

    @FXML
    private TextField username;
    @FXML
    private PasswordField userPassword;
    @FXML
    private Button SignIn;
    @FXML
    private Button Apply;
    
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    @FXML
    private Label login_status;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void Login(ActionEvent event) throws ClassNotFoundException, SQLException, FileNotFoundException {
        
        String user  = username.getText();
        String pass = userPassword.getText();
        
 
        
        // Load the Driver
        Class.forName("com.mysql.cj.jdbc.Driver");
        System.out.println("Driver Connected");
        
        // Connect to database
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/study_guide?serverTimezone=UTC", "root", "password");
        System.out.println("Database Connected");
        
        // Construct query
        String queryString="Select * FROM users WHERE username= ? AND secret = ?";
              
        
           try{
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1, user);
            preparedStatement.setString(2, pass);
            resultSet = preparedStatement.executeQuery();
            
               if(!resultSet.next()){
                
                   login_status.setText("Please try again!");
                   login_status.setVisible(true);
                }
            
               else{
                 
                   // Close the connection
                    connection.close();
                    PrintWriter output= new PrintWriter(new File("current_user.txt"));
                    output.write(user);
                    output.close();
                   ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
                  
                   
                    Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
                    Image icon = new Image(getClass().getResourceAsStream("/images/graduation.png"));
                    Stage stage = new Stage();
                    stage.getIcons().add(icon);
                    Scene scene = new Scene(root);
        
                    //The style sheet sets parameters for certain object id's or classes
        
                    scene.getStylesheets().addAll(this.getClass().getResource("sheet1.css").toExternalForm());
                    stage.setScene(scene);
                    stage.setTitle("Study Guide");
                    stage.show();
                }
        }
             catch(Exception e){
                    e.printStackTrace();
                }
        
       
    }

    @FXML
    private void Register(ActionEvent event) throws IOException {
        
        System.out.println("New Registration");
        
         Parent root = FXMLLoader.load(getClass().getResource("Register.fxml"));
                    Image icon = new Image(getClass().getResourceAsStream("/images/graduation.png")); 
                    Stage stage = new Stage();
                    stage.getIcons().add(icon);
                    Scene scene = new Scene(root);
        
                    //The style sheet sets parameters for certain object id's or classes
        
                    scene.getStylesheets().addAll(this.getClass().getResource("register.css").toExternalForm());
                    stage.setScene(scene);
                    stage.setTitle("Study Guide");
                    stage.show();
        
        
    }
    
}
