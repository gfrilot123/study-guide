package studyguide;

/**
 *
 * @author geraldfrilot
 */
public class History {

    public String[] Questions() {
        String[] questions = {"In 1869, President Ulysses S. Grant announced a new peace policy ", "Populists in western states endorsed women suffrage",
            "In 1880, new immigrants were welcome by the American people",
            "In the Progressive era, industry was on the rise and agriculture was on decline", "Christopher Columbus sailed the ocean blue in 1492"};

        return questions;
    }

    public String[] Answers() {
        String[] answers = {"True", "True", "False", "False", "True"};

        return answers;
    }
}
