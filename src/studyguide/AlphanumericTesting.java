
package studyguide;
import java.util.Comparator;
import org.junit.runner.Description;
import org.junit.runner.manipulation.Ordering;


public class AlphanumericTesting  implements Ordering.Factory{
    

    public Object create(Object context) {
        return this;
    }

    private static final Comparator<Description> COMPARATOR = new Comparator<Description>() {
        public int compare(Description o1, Description o2) {
            return o1.getDisplayName().compareTo(o2.getDisplayName());
        }
    };

    @Override
    public Ordering create(Ordering.Context cntxt) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
    
}
