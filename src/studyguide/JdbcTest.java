/*
 * 
 */
package studyguide;
import java.sql.*;

/**
 *
 * @author geraldfrilot
 */
public class JdbcTest  {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        // Loading the JDBC Driver
        Class.forName("com.mysql.cj.jdbc.Driver");
        System.out.println("Driver Connected");
        
        // Connect to database
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/study_guide?serverTimezone=UTC", "root", "password");
        System.out.println("Database Connected");
        
        // Create statement
        Statement statement = connection.createStatement();
        
        String queryString="Insert into users VALUES('1','Gerald','Frilot',35,'male','gfrilot','free','kinetic')";
        //Execute a statement
        statement.executeUpdate(queryString);
        
        connection.close();
    }
    
}
