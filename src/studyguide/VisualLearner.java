/*
 * 
 */
package studyguide;
import java.util.ArrayList;

/**
 *
 * @author geraldfrilot
 */
public class VisualLearner {
    
     ArrayList corr_arr = new ArrayList();
      ArrayList missed_arr = new ArrayList();
     
    
    protected void MyLearningStyle(String name)
    {
        System.out.println("Your learning style selected is: "+name);
    }
    
    protected ArrayList TotalCorrect(int  results_correct , String learning_phase)
    {
       corr_arr.add(results_correct);
       corr_arr.add(learning_phase);
        
        return corr_arr;
    }
    
    
    
    protected ArrayList TotalMissed(int results_missed , String learning_phase)
    {
         missed_arr.add(results_missed);
         missed_arr.add(learning_phase);
        
         return  missed_arr;
    }
    
    
    
    protected double LearningCombination(double visual,double kinetic, double audible ,double verbal)
    {
        double average_learner = (visual + kinetic + audible + verbal)/4;
        
        return average_learner;
    }
    
    
    
}
