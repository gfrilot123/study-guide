/*
 * 
 */
package studyguide;

/**
 *
 * @author geraldfrilot
 */
public class VisualLearner {
    
    
    
    protected void MyLearningStyle(String name)
    {
        //
    }
    
    protected int TotalCorrect(int results_correct)
    {
        return results_correct;
    }
    
    
    
    protected int TotalMissed(int results_missed)
    {
        return results_missed;
    }
    
    
    
    protected double LearningCombination(double visual,double kinetic, double audible ,double verbal)
    {
        double average_learner = (visual + kinetic + audible + verbal)/4;
        
        return average_learner;
    }
    
}
