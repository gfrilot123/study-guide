
/*
 * 
 * 
 */
package studyguide;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.util.Calendar;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
/**
 *
 * @author mac
 */
public class FXMLDocumentController implements Initializable {

    
    /**
     * A grid pane is better for resizing a window compared to an AnchorPane.
     * An AnchorPane keeps everything in a fix positioned except the background image
     */
    
    @FXML
    private GridPane gp;
    @FXML
    private Button b1;
    
    
    Stage newProjectStage;
    Calendar today;
    int count=0;
    
    
    @FXML
    private Hyperlink googleSearch;
    @FXML
    private Hyperlink G4G;
    @FXML
    private DatePicker datePick;
    @FXML
    private Label timeLabel;
    
    
   /**
    * The circle class is instantiated here and  a radius is selected of 1.5
    * The max width and height are set by passing parameters to setMaxSize
    * @param url
    * @param rb 
    * 
    */ 
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       b1.setShape(new Circle(1.5));
       b1.setMaxSize(75,75);
       b1.setFocusTraversable(true);
       showTime();
       
      }   
    
    

    @FXML
    private void selectFile(ActionEvent event) throws IOException {
        Stage searchStage = new Stage();
        searchStage.centerOnScreen();
        searchStage.setTitle("Search");
        FileChooser fc = new FileChooser();
        File selectedFile = fc.showOpenDialog(searchStage);
        if(selectedFile!=null){
      
        Desktop.getDesktop().open(selectedFile);
        
        }
        
        
        
    }

    @FXML
    private void openMoodle(ActionEvent event) throws URISyntaxException, IOException {
        URI ull = new URI("https://www.louisiana.edu/");
        Desktop.getDesktop().browse(ull);
        
    }

    @FXML
    private void newWindow(ActionEvent event) {
     try {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("newProjectFXML.fxml"));
                Parent root3 = (Parent) fxmlLoader.load();
                newProjectStage = new Stage();
                Scene p1Scene = new Scene(root3,1000,880);
                p1Scene.getStylesheets().addAll(this.getClass().getResource("newprojectfxml.css").toExternalForm());
                newProjectStage.setScene(p1Scene);  
                newProjectStage.show();
                
        } catch(Exception e) {
           e.printStackTrace();
          }
    
}

    @FXML
    private void browseGoogle(ActionEvent event) throws URISyntaxException, IOException {
        URI goo = new URI("https://www.google.com/");
        Desktop.getDesktop().browse(goo);
    }

    @FXML
    private void browseStkovf(ActionEvent event) throws URISyntaxException, IOException {
        URI stk = new URI("https://stackoverflow.com/#");
        Desktop.getDesktop().browse(stk);
    }

    @FXML
    private void geekSearch(ActionEvent event) throws URISyntaxException, IOException {
        URI stk = new URI("https://www.geeksforgeeks.org/");
        Desktop.getDesktop().browse(stk);
    }
    
    private void showTime(){
        
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0),event -> timeLabel.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss a")))),
                                         new KeyFrame(Duration.seconds(1)));

        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
    
    
    }
    
 
