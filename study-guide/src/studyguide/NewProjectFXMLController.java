/*
 * 
 * 
 */
package studyguide;


import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
import java.util.Scanner;
import javafx.scene.control.Button;
import javafx.scene.control.Label;


/**
 *
 * @author mac
 */
public class NewProjectFXMLController implements Initializable {
    
   
    Scanner input;
    int count;
    ArrayList<String> alist; 
       
    @FXML
    private GridPane p1Grid;
    @FXML
    private ToggleButton toggleQuestionButton;
    @FXML
    private TextField flashcardQuestion;
    @FXML
    private TextField fileLocationField;
    @FXML
    private Label fileFoundLabel;
    @FXML
    private Button enterFile;

    /**
     * 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       toggleQuestionButton.setShape(new Circle(1.5));
       toggleQuestionButton.setMaxSize(55,55);
       count=0;
       toggleQuestionButton.setVisible(false);
       
    }    


    @FXML
    private void switchQuestion(ActionEvent event) {
        
      
          if(count >= alist.size()){
             count=0;
         
         }
         flashcardQuestion.setText(alist.get(count));
        
         count++;
         
      
        
    }

    @FXML
    private void verifyFile(ActionEvent event) {
        
        
        
         try {
            input= new Scanner((new File(fileLocationField.getText())));
            toggleQuestionButton.setVisible(true);
            fileFoundLabel.setText("");
            
        } catch (FileNotFoundException ex) {
            fileFoundLabel.setText("File Does Not Exist, Please Try Again");
            ex.printStackTrace();
            
         }
         
           alist = new ArrayList<>();
       
         while(input.hasNextLine()){
            
            alist.add(input.nextLine());
             
         
         }
    
    }
    

    
}
