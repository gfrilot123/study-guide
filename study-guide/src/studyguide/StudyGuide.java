
/*
 * 
 */
package studyguide;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author mac
 */
public class StudyGuide extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Image icon = new Image(getClass().getResourceAsStream("/images/graduation.png")); 
        stage.getIcons().add(icon);
        Scene scene = new Scene(root);
        
        //The style sheet sets parameters for certain object id's or classes
        
        scene.getStylesheets().addAll(this.getClass().getResource("sheet1.css").toExternalForm());
        stage.setScene(scene);
        stage.setTitle("Study Guide");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
